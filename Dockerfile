FROM maven:3.6.2-jdk-8-slim AS MAVEN_BUILD

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/

RUN mvn clean install -DskipTests

FROM openjdk:8-jre

WORKDIR /spring-boot-data-jpa

# # environment variable with default value
ENV DB_USER=$DB_USER
ENV DB_PWD=$DB_PWD

COPY --from=MAVEN_BUILD /build/target/spring-boot-data-jpa-0.0.1-SNAPSHOT.jar /spring-boot-data-jpa/

ENTRYPOINT ["java","-jar","-Djasypt.encryptor.password=dbencryptpwdkey","-Ddb.username=${DB_USER}","-Ddb.pwd=${DB_PWD}","spring-boot-data-jpa-0.0.1-SNAPSHOT.jar"]